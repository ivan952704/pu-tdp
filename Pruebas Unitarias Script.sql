CREATE DATABASE [PruebasUnitarias]
GO
USE [PruebasUnitarias]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 26/03/2016 12:22:25 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categorias](
	[CategoriaId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Descripcion] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[CategoriaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 26/03/2016 12:22:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[ProductoId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [varchar](max) NOT NULL,
	[CategoriaId] [int] NOT NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[ProductoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Categorias] ON 

INSERT [dbo].[Categorias] ([CategoriaId], [Nombre], [Descripcion]) VALUES (5, N'Chocolates', N'Dulce hecho de cacao.')
INSERT [dbo].[Categorias] ([CategoriaId], [Nombre], [Descripcion]) VALUES (6, N'Gaseosas', N'Bebida con alto nivel de azucar.')
INSERT [dbo].[Categorias] ([CategoriaId], [Nombre], [Descripcion]) VALUES (7, N'Snacks', N'Bocados salados o dulces.')
SET IDENTITY_INSERT [dbo].[Categorias] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([ProductoId], [Nombre], [Precio], [Cantidad], [Descripcion], [CategoriaId]) VALUES (87, N'Producto Nombre 1', CAST(2.40 AS Decimal(18, 2)), 240, N'Producto Descripcion 1', 6)
INSERT [dbo].[Productos] ([ProductoId], [Nombre], [Precio], [Cantidad], [Descripcion], [CategoriaId]) VALUES (88, N'Producto Nombre 2', CAST(4.80 AS Decimal(18, 2)), 480, N'Producto Descripcion 2', 5)
INSERT [dbo].[Productos] ([ProductoId], [Nombre], [Precio], [Cantidad], [Descripcion], [CategoriaId]) VALUES (89, N'Producto Nombre 3', CAST(9.60 AS Decimal(18, 2)), 960, N'Producto Descripcion 3', 6)
INSERT [dbo].[Productos] ([ProductoId], [Nombre], [Precio], [Cantidad], [Descripcion], [CategoriaId]) VALUES (90, N'Producto Nombre 4', CAST(19.20 AS Decimal(18, 2)), 1920, N'Producto Descripcion 4', 5)
INSERT [dbo].[Productos] ([ProductoId], [Nombre], [Precio], [Cantidad], [Descripcion], [CategoriaId]) VALUES (91, N'Producto Nombre 5', CAST(38.40 AS Decimal(18, 2)), 3840, N'Producto Descripcion 5', 7)
SET IDENTITY_INSERT [dbo].[Productos] OFF
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([CategoriaId])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_Categorias]
GO
/****** Object:  StoredProcedure [dbo].[pruebasunitarias_ins_producto]    Script Date: 26/03/2016 12:22:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pruebasunitarias_ins_producto]
(
	@nombre				varchar(200),
	@precio				decimal(18, 2),
	@cantidad			int,
	@descripcion		varchar(max),
	@categoriaId		int
)
as
begin
	insert into Productos(Nombre, Precio, Cantidad, Descripcion, CategoriaId)
	values(@nombre, @precio, @cantidad, @descripcion, @categoriaId)
end

GO
/****** Object:  StoredProcedure [dbo].[pruebasunitarias_sel_categorias]    Script Date: 26/03/2016 12:22:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[pruebasunitarias_sel_categorias]
as
begin
	select	C.CategoriaId, C.Nombre, C.Descripcion
	from	Categorias C
end
GO
/****** Object:  StoredProcedure [dbo].[pruebasunitarias_sel_productos]    Script Date: 26/03/2016 12:22:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[pruebasunitarias_sel_productos]
as
begin
	select	P.ProductoId, P.Nombre, P.Precio, 
			P.Cantidad, P.Descripcion, P.CategoriaId
	from	Productos P
end

GO
