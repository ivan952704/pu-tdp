﻿using System.Collections.Generic;
using PruebasUnitarias.Entity.Entity;
using PruebasUnitarias.Mvc.ViewModel.Base;

namespace PruebasUnitarias.Mvc.ViewModel.Producto
{
    public class ProductosViewModel : BaseViewModel
    {
        public List<ProductoDto> LstProductosDto { get; set; }

        public void CargarDatos()
        {
            LstProductosDto = ObjProductoBusiness.ListarProductosConCategorias();
        }
    }
}
