﻿using System;
using System.ComponentModel.DataAnnotations;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Mvc.ViewModel.Producto
{
    public class ProductoViewModel
    {
        public Int32 ProductoId { get; set; }

        [Required]
        public Int32 CategoriaId { get; set; }

        [Required]
        public String Nombre { get; set; }

        [Required]
        public String Descripcion { get; set; }

        [Required]
        public Decimal Precio { get; set; }

        [Required]
        public Int32 Cantidad { get; set; }

        public CategoriaDto CategoriaDto { get; set; }
    }
}
