﻿using PruebasUnitarias.Business.Business;

namespace PruebasUnitarias.Mvc.ViewModel.Base
{
    public class BaseViewModel
    {
        protected ProductoBusiness ObjProductoBusiness;

        public BaseViewModel()
        {
            ObjProductoBusiness = ProductoBusiness.ObtenerInstancia();
        }
    }
}
