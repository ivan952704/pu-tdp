﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PruebasUnitarias.Mvc.Startup))]
namespace PruebasUnitarias.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
