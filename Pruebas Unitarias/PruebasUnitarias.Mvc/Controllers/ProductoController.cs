﻿using System;
using System.Web.Mvc;
using PruebasUnitarias.Entity.Entity;
using PruebasUnitarias.Mvc.ViewModel.Producto;

namespace PruebasUnitarias.Mvc.Controllers
{
    public class ProductoController : BaseController
    {
        public ActionResult ListarProductos()
        {
            var viewModel = new ProductosViewModel();
            viewModel.CargarDatos();
            return View(viewModel);
        }

        public ActionResult RegistrarProducto()
        {
            ProductoViewModel viewModel = new ProductoViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult RegistrarProducto(ProductoViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TryUpdateModel(viewModel);
                    TempData["MensajeRespuesta"] = "Por favor, complete los campos requeridos.";
                    return View(viewModel);
                }

                ProductoDto productoDto = new ProductoDto
                {
                    CategoriaId = viewModel.CategoriaId,
                    Nombre = viewModel.Nombre,
                    Descripcion = viewModel.Descripcion,
                    Cantidad = viewModel.Cantidad,
                    Precio = viewModel.Precio
                };

                ObjProductoBusiness.Insertar(productoDto);
                TempData["MensajeRespuesta"] = "El producto fue creado exitosamente.";
            }
            catch (Exception e)
            {
                TryUpdateModel(viewModel);
                TempData["MensajeRespuesta"] = 
                    "Error durante la creación del producto, vuelva a intentarlo.";
                return View(viewModel);
            }

            return RedirectToAction("ListarProductos");
        }
    }
}
