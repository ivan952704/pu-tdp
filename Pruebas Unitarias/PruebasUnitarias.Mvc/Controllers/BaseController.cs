﻿using System.Web.Mvc;
using PruebasUnitarias.Business.Business;

namespace PruebasUnitarias.Mvc.Controllers
{
    public class BaseController : Controller
    {
        protected ProductoBusiness ObjProductoBusiness;

        public BaseController()
        {
            ObjProductoBusiness = ProductoBusiness.ObtenerInstancia();
        }
    }
}
