﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PruebasUnitarias.Business.Business;
using PruebasUnitarias.Mvc.Controllers;
using PruebasUnitarias.Mvc.ViewModel.Producto;

namespace PruebasUnitarias.Tests.Producto
{
    [TestClass]
    public class ProductoTest
    {
        [TestMethod]
        public void TestListarProducto()
        {
            var productoBusiness = ProductoBusiness.ObtenerInstancia();
            var lstProductos = productoBusiness.ListarProductosConCategorias();
            Assert.IsNotNull(lstProductos);
        }

        [TestMethod]
        public void TestEliminarTodo()
        {
            var productoBusiness = ProductoBusiness.ObtenerInstancia();
            productoBusiness.EliminarTodo();
            var lstProductos = productoBusiness.ListarProductosConCategorias();
            Assert.AreEqual(0, lstProductos.Count);
        }

        [TestMethod]
        [ExpectedException(typeof (System.ArgumentNullException))]
        public void TestInsertarProductoError()
        {
            var controller = new ProductoController();
            controller.RegistrarProducto(new ProductoViewModel());
        }

        [TestMethod]
        public void TestProductoViewModel()
        {
            var viewModel = new ProductoViewModel
            {
                CategoriaId = 5,
                Cantidad = 120,
                Precio = Decimal.Parse("1.20")
            };

            var contexto = new ValidationContext(viewModel);
            var lstValidaciones = new List<ValidationResult>();
            var esValido = Validator.TryValidateObject(viewModel, contexto, lstValidaciones);

            if (esValido)
            {
                Assert.Fail("El ViewModel es válido.");
            }
            else
            {
                String mensaje = "Error en el ViewModel: ";
                mensaje = lstValidaciones.Aggregate(mensaje, (current, item) => current + (item + ", "));
                System.Diagnostics.Trace.WriteLine(mensaje);
                Assert.IsFalse(esValido, mensaje);
            }
        }

        [TestMethod]
        public void TestInsertarProducto()
        {
            var controller = new ProductoController();
            var random = new Random();
            int cantidad = 5;

            for (int i = 1; i <= cantidad; i++)
            {
                var viewModel = new ProductoViewModel
                {
                    Descripcion = "Producto Descripcion " + i,
                    Nombre = "Producto Nombre " + i,
                    Cantidad = i * 20,
                    Precio = Decimal.Parse((i * 1.40).ToString()),
                    CategoriaId = random.Next(5, 8)
                };
                controller.RegistrarProducto(viewModel);
            }

            var productoBusiness = ProductoBusiness.ObtenerInstancia();
            var lstProductos = productoBusiness.ListarProductosConCategorias();
            Assert.IsTrue(lstProductos.Count == cantidad);
        }
    }
}
