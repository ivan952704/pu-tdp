﻿using System;

namespace PruebasUnitarias.Entity.Entity
{
    public class ProductoDto
    {
        public Int32 ProductoId { get; set; }
        public Decimal Precio { get; set; }
        public Int32 Cantidad { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public CategoriaDto CategoriaDto { get; set; }
        public Int32 CategoriaId { get; set; }
    }
}
