﻿using System;

namespace PruebasUnitarias.Entity.Entity
{
    public class CategoriaDto
    {
        public Int32 CategoriaId { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
    }
}
