﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebasUnitarias.Business.Base;
using PruebasUnitarias.Dao.Dao.Ado;
using PruebasUnitarias.Dao.Interfaces;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Business.Business
{
    public sealed class ProductoBusiness : IBaseBusiness<ProductoDto>
    {
        #region Bloque Singleton

        private static ProductoBusiness PRODUCTO_BUSINESS = new ProductoBusiness();
        private IProductoDao _productoDao;
        private ICategoriaDao _categoriaDao;

        private ProductoBusiness()
        {
            _productoDao = ProductoAdo.ObtenerInstancia();
            _categoriaDao = CategoriaAdo.ObtenerInstancia();
        }

        public static ProductoBusiness ObtenerInstancia()
        {
            return PRODUCTO_BUSINESS;
        }

        #endregion

        #region Metodos de Interfaz

        public void Insertar(ProductoDto entity)
        {
            try
            {
                _productoDao.Insertar(entity);
            }
            catch (Exception e)
            {
                throw new Exception("ProductoBusiness - Insertar: " + e.Message, e);
            }
        }

        public List<ProductoDto> Listar()
        {
            try
            {
                return _productoDao.Listar();
            }
            catch (Exception e)
            {
                throw new Exception("ProductoBusiness - Listar: " + e.Message, e);
            }
        }

        #endregion

        #region Metodos Personalizados

        public void EliminarTodo()
        {
            try
            {
                _productoDao.EliminarTodo();
            }
            catch (Exception e)
            {
                throw new Exception("ProductoBusiness - EliminarTodo: " + e.Message, e);
            }
        }

        public List<ProductoDto> ListarProductosConCategorias()
        {
            List<ProductoDto> lstProductosDtoResultado = null;

            try
            {
                var lstProductosDto = _productoDao.Listar();
                var lstCategoriasDto = _categoriaDao.Listar();

                lstProductosDtoResultado = lstProductosDto.
                    OrderBy(x => x.Nombre).
                    Join(
                        lstCategoriasDto.AsEnumerable(),
                        p => p.CategoriaId,
                        c => c.CategoriaId,
                        (p, c) => new ProductoDto
                        {
                            ProductoId = p.ProductoId,
                            Nombre = p.Nombre.ToUpper(),
                            Cantidad = p.Cantidad,
                            Precio = p.Precio,
                            Descripcion = p.Descripcion,
                            CategoriaId = p.CategoriaId,
                            CategoriaDto = new CategoriaDto
                            {
                                CategoriaId = p.CategoriaId,
                                Nombre = c.Nombre,
                                Descripcion = c.Descripcion
                            }
                        }
                    ).ToList();
            }
            catch (Exception e)
            {
                throw new Exception("ProductoBusiness - ListarProductosConCategorias: " + e.Message, e);
            }
            return lstProductosDtoResultado;
        }

        #endregion
    }
}
