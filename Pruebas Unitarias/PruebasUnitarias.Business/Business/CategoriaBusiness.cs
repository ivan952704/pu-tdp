﻿using System;
using System.Collections.Generic;
using PruebasUnitarias.Business.Base;
using PruebasUnitarias.Dao.Dao.Ado;
using PruebasUnitarias.Dao.Interfaces;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Business.Business
{
    public sealed class CategoriaBusiness : IBaseBusiness<CategoriaDto>
    {
        #region Bloque Singleton

        private static CategoriaBusiness CATEGORIA_BUSINESS = new CategoriaBusiness();
        private ICategoriaDao _categoriaDao;

        private CategoriaBusiness()
        {
            _categoriaDao = CategoriaAdo.ObtenerInstancia();
        }

        public static CategoriaBusiness ObtenerInstancia()
        {
            return CATEGORIA_BUSINESS;
        }

        #endregion

        #region Metodos de Interfaz

        public void Insertar(CategoriaDto entity)
        {
            throw new NotImplementedException();
        }

        public List<CategoriaDto> Listar()
        {
            try
            {
                return _categoriaDao.Listar();
            }
            catch (Exception e)
            {
                throw new Exception("CategoriaBusiness - Listar: " + e.Message, e);
            }
        }

        #endregion
    }
}
