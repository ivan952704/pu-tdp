﻿using System.Collections.Generic;

namespace PruebasUnitarias.Business.Base
{
    public interface IBaseBusiness<T>
    {
        void Insertar(T entity);
        List<T> Listar();
    }
}
