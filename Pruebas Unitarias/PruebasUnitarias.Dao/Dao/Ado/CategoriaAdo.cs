﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using PruebasUnitarias.Dao.Base;
using PruebasUnitarias.Dao.Interfaces;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Dao.Dao.Ado
{
    public sealed class CategoriaAdo : Conexion, ICategoriaDao
    {
        #region Bloque Singleton

        private static CategoriaAdo CATEGORIA_ADO = new CategoriaAdo();

        private CategoriaAdo()
        {
        }

        public static CategoriaAdo ObtenerInstancia()
        {
            return CATEGORIA_ADO;
        }

        #endregion

        #region Metodos de Interfaz

        public void Insertar(CategoriaDto entity)
        {
            throw new NotImplementedException();
        }

        public List<CategoriaDto> Listar()
        {
            List<CategoriaDto> lstCategoriasDto = new List<CategoriaDto>();
            DataTable dtCategorias = new DataTable("Categorias");

            try
            {
                SqlCon = ObtenerConexion();
                SqlCmd = new SqlCommand
                {
                    Connection = SqlCon,
                    CommandText = "dbo.pruebasunitarias_sel_categorias",
                    CommandType = CommandType.StoredProcedure
                };

                SqlDataAdapter sqlDat = new SqlDataAdapter(SqlCmd);
                sqlDat.Fill(dtCategorias);

                lstCategoriasDto.AddRange
                    (
                        dtCategorias.Rows.Cast<DataRow>().
                            Select(fila => new CategoriaDto
                            {
                                CategoriaId = Int32.Parse(fila["CategoriaId"].ToString()),
                                Nombre = fila["Nombre"].ToString(),
                                Descripcion = fila["Descripcion"].ToString()
                            })
                    );
            }
            catch (Exception e)
            {
                throw new Exception("CategoriaAdo - Listar: " + e.Message, e);
            }
            finally
            {
                if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }

            return lstCategoriasDto;
        }

        #endregion
    }
}
