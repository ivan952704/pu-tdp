﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebasUnitarias.Dao.Base;
using PruebasUnitarias.Dao.Interfaces;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Dao.Dao.Ado
{
    public sealed class ProductoAdo : Conexion, IProductoDao
    {
        #region Bloque Singleton

        private static ProductoAdo PRODUCTO_ADO = new ProductoAdo();

        private ProductoAdo()
        {
        }

        public static ProductoAdo ObtenerInstancia()
        {
            return PRODUCTO_ADO;
        }

        #endregion

        #region Metodos de Interfaz

        public void Insertar(ProductoDto entity)
        {
            try
            {
                SqlCon = ObtenerConexion();
                SqlCon.Open();
                SqlCmd = new SqlCommand
                {
                    Connection = SqlCon,
                    CommandText = "dbo.pruebasunitarias_ins_producto",
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter sqlParamCategoriaId = new SqlParameter
                {
                    ParameterName = "@categoriaId",
                    SqlDbType = SqlDbType.Int,
                    Value = entity.CategoriaId
                };
                SqlCmd.Parameters.Add(sqlParamCategoriaId);

                SqlParameter sqlParamNombre = new SqlParameter
                {
                    ParameterName = "@nombre",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 200,
                    Value = entity.Nombre
                };
                SqlCmd.Parameters.Add(sqlParamNombre);

                SqlParameter sqlParamPrecio = new SqlParameter
                {
                    ParameterName = "@precio",
                    SqlDbType = SqlDbType.Decimal,
                    Precision = 18,
                    Scale = 2,
                    Value = entity.Precio
                };
                SqlCmd.Parameters.Add(sqlParamPrecio);

                SqlParameter sqlParamCantidad = new SqlParameter
                {
                    ParameterName = "@cantidad",
                    SqlDbType = SqlDbType.Int,
                    Value = entity.Cantidad
                };
                SqlCmd.Parameters.Add(sqlParamCantidad);

                SqlParameter sqlParamDescripcion = new SqlParameter
                {
                    ParameterName = "@descripcion",
                    SqlDbType = SqlDbType.VarChar,
                    Size = -1,
                    Value = entity.Descripcion
                };
                SqlCmd.Parameters.Add(sqlParamDescripcion);

                SqlCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("ProductoAdo - Insertar: " + e.Message, e);
            }
            finally
            {
                if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
        }

        public List<ProductoDto> Listar()
        {
            List<ProductoDto> lstProductosDto = new List<ProductoDto>();
            DataTable dtProductos = new DataTable("Productos");

            try
            {
                SqlCon = ObtenerConexion();
                SqlCmd = new SqlCommand
                {
                    Connection = SqlCon,
                    CommandText = "dbo.pruebasunitarias_sel_productos",
                    CommandType = CommandType.StoredProcedure
                };

                SqlDataAdapter sqlDat = new SqlDataAdapter(SqlCmd);
                sqlDat.Fill(dtProductos);

                lstProductosDto.AddRange
                    (
                        dtProductos.Rows.Cast<DataRow>().
                            Select(fila => new ProductoDto
                            {
                                ProductoId = Int32.Parse(fila["ProductoId"].ToString()),
                                Cantidad = Int32.Parse(fila["Cantidad"].ToString()),
                                Precio = Decimal.Parse(fila["Precio"].ToString()),
                                Nombre = fila["Nombre"].ToString(),
                                Descripcion = fila["Descripcion"].ToString(),
                                CategoriaId = Int32.Parse(fila["CategoriaId"].ToString()),
                            })
                    );
            }
            catch (Exception e)
            {
                throw new Exception("ProductoAdo - Listar: " + e.Message, e);
            }
            finally
            {
                if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }

            return lstProductosDto;
        }

        #endregion

        #region Metodos Personalizados

        public void EliminarTodo()
        {
            try
            {
                SqlCon = ObtenerConexion();
                SqlCon.Open();

                SqlCmd = new SqlCommand
                {
                    Connection = SqlCon,
                    CommandText = "delete from Productos",
                    CommandType = CommandType.Text
                };

                SqlCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("ProductoAdo - EliminarTodo: " + e.Message, e);
            }
            finally
            {
                if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
        }

        #endregion
    }
}
