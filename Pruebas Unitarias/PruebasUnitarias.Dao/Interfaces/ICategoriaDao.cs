﻿using PruebasUnitarias.Entity.Entity;
using PruebasUnitarias.Dao.Base;

namespace PruebasUnitarias.Dao.Interfaces
{
    public interface ICategoriaDao : IBaseDao<CategoriaDto>
    {
    }
}
