﻿using PruebasUnitarias.Dao.Base;
using PruebasUnitarias.Entity.Entity;

namespace PruebasUnitarias.Dao.Interfaces
{
    public interface IProductoDao : IBaseDao<ProductoDto>
    {
        void EliminarTodo();
    }
}
