﻿using System.Data.SqlClient;

namespace PruebasUnitarias.Dao.Base
{
    public class Conexion
    {
        protected SqlConnection SqlCon;
        protected SqlCommand SqlCmd;

        protected SqlConnection ObtenerConexion()
        {
            return new SqlConnection
            {
                ConnectionString = "Server=localhost; Database=PruebasUnitarias; " +
                                   "Integrated Security=SSPI;"
            };
        }
    }
}
