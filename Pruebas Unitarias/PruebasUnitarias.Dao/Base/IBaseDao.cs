﻿using System.Collections.Generic;

namespace PruebasUnitarias.Dao.Base
{
    public interface IBaseDao<T>
    {
        void Insertar(T entity);
        List<T> Listar();
    }
}
